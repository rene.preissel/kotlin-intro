package de.e2.intro.tests

import com.natpryce.hamkrest.absent
import com.natpryce.hamkrest.and
import com.natpryce.hamkrest.assertion.assert
import com.natpryce.hamkrest.equalTo
import com.natpryce.hamkrest.present
import org.junit.Test


fun sum(a: Int?, b: Int?): Int? = when {
    a != null && b != null -> a + b
    a != null -> a
    b != null -> b
    else -> null
}


class NumberTest {

    @Test
    @JvmName("add")
    fun `add positive numbers`() {
        val c = sum(4,5)
        assert.that(c, present<Int>() and equalTo(9))
    }

    @Test
    fun `add null numbers`() {
        val c = sum(null,null)
        assert.that(c, absent())
    }
}