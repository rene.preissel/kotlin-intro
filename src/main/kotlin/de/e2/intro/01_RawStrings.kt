package de.e2.intro.raw

fun main(args: Array<String>) {
    val testString1 = """
        Hier kann man 
        einen String über mehrere Zeilen
        schreiben.
        Sehr gut geeignet für Tests.
        """

    println(testString1)


    val testString2 = """
        |trimMargin() entfernt
        |die führenden Whitespaces
        |und das Pipe-Symbol.
        |Das Symbol ist frei wählbar.
        """.trimMargin()

    println(testString2)
}