package de.e2.intro.dataclass


data class Circle(val x: Int = 0, val y: Int = 0, val radius: Int = 0)

fun main(args: Array<String>) {
    val circle1 = Circle(radius = 10)
    val circle2 = Circle(x = 1, y = 1)

    //== ruft equals auf,  === vergleicht Identität
    if (circle1 == circle2) {
        println("Inhaltlich gleich")
    }

    //Nur die übergebenen Properties werden überschrieben, der Rest wird kopiert
    val circle3 = circle1.copy(x = 10, y = 10)

    //Destructuring - funktioniert nach Position
    val (x, y) = circle3


    val map: Map<Int, Circle> = mapOf(1 to circle1, 2 to circle2, 3 to circle3)

    //Destructuring klappt auch bei Map-Entries
    for ((key, value) in map) {
        // data classes haben auch eine toString-Methode
        println("$key = $value")
    }

    //Mit Destructing können Funktionen mehrere Ergebnisse zurückliefern
    val (quotient1, rest1) = divide(10, 3)

    //Meistens ist es besser eine eigene Data-Class zu verwenden
    val (quotient2, rest2) = divideBetter(10, 3)
}

fun divide(a: Int, b: Int) = Pair(a / b, a % b) // geht auch: (a / b) to (a % b)

data class DivideResult(val quotient: Int, val rest: Int)

fun divideBetter(a: Int, b: Int) = DivideResult(a / b, a % b)

