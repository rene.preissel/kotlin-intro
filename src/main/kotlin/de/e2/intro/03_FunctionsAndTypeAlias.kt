package de.e2.intro.functions


fun main(args: Array<String>) {
    val circle1 = Circle(10) //??
    val circle2 = Circle(radius = 10) // Parameter können mit Namen angegeben werden
    val circle3 = Circle(x = 1, y = 1) // Besonders sinvoll wenn es die gleichen Typen sind
    val circle4 = circle3 * 2 //Operator overloading

    if(circle1 > circle2) {

    }

    //Infix Funktionen benötigen keinen Punkt und keine Klammer
    val pair: Pair<Int, Circle> = 1 to circle1

    //TypeAlias für bessere Lesbarkeit
    val map: IdCircleMap = mapOf(1 to circle1, 2 to circle2, 3 to circle3)


    val praxis: PraxisID ="10"
    var patient: PatientID = "20"

    patient=praxis
}

class Circle(
    val x: Int = 0,
    val y: Int = 0,
    val radius: Int = 0
) : Comparable<Circle> {
    override fun compareTo(other: Circle): Int {
        return this.radius-other.radius
    }

    //*-Operator definieren
    operator fun times(value: Int): Circle = Circle(x, y, radius * value)
}


typealias IdCircleMap = Map<Int, Circle>
typealias PatientID = String
typealias PraxisID = String



