package de.e2.intro.expressions


enum class Geschlecht {
    MANN, FRAU
}

fun main(args: Array<String>) {
    val geschlecht = Geschlecht.MANN

    //If ist eine Expression, deswegen gibt es auch keinen tertiären Operator ?
    val anrede = if (geschlecht == Geschlecht.MANN) "Herr" else "Frau"

    //try ist auch eine Expression
    val numberOrMinusOne = try {
        Integer.parseInt("abc")
    } catch (exc: NumberFormatException) {
        -1
    }


    //when is ein besseres "switch", kein break notwendig
    val alter = 45
    val altersText = when (alter) {
        0, 1 -> "Säugling"
        2, 3 -> "Kleinkind"
        in 4..14 -> "Kind"
        in 15..18 -> "Jugendlicher"
        else -> "Erwachsener"
    }

    //when geht auch ohne übergebene Variable
    val a: Int? = 4
    val b: Int? = 5
    val c = when {
        a != null && b != null -> a + b
        a != null -> a
        b != null -> b
        else -> null
    }
}

