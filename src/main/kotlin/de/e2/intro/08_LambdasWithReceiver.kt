package de.e2.intro.receiver


data class Validated<T>(val result: T?, val errors: List<String>, val warnings: List<String>)

fun main(args: Array<String>) {
    val alter = 45
    //So könnte Valaidation aussehen
    val validated: Validated<String> = withValidation {
        if (alter > 80) {
            //Fehler registrieren
            error("zu alt")
            stopValidation()
            println("Unreachable") //gibt auch eine Warnung
        }
        if (alter == 18) {
            warn("passt gerade")
        }
        "Fachliches Ergebnis"
    }

    println(validated)

    //Chaining von Validation
    val nextValidated = validated.map { value ->
        if (value.length > 10) {
            error("Zu lang")
            stopValidation()
        }
        value.length
    }

    println(nextValidated)
}


class ValidationContext {
    internal class StopValidationException : RuntimeException()

    val errors = mutableListOf<String>()
    val warnings = mutableListOf<String>()

    fun error(error: String) {
        errors += error
    }

    fun warn(warn: String) {
        warnings += warn
    }

    fun stopValidation(): Nothing {
        throw StopValidationException()
    }
}

fun <T> withValidation(block: ValidationContext.() -> T?): Validated<T> {
    val collector = ValidationContext()
    val result = try {
        collector.block() //block(collector)
    } catch (ex: ValidationContext.StopValidationException) {
        null
    }
    if (collector.errors.isEmpty()) {
        return Validated(result, collector.errors.toList(), collector.warnings.toList())
    }
    return Validated(null, collector.errors.toList(), collector.warnings.toList())
}

//Damit ist auch Chaining von Validated möglich
fun <T, E> Validated<T>.map(block: ValidationContext.(T) -> E?): Validated<E> {
    if (result == null) {
        return Validated(null, errors, warnings)
    }
    val collector = ValidationContext()
    collector.errors += errors
    collector.warnings += warnings
    val result = try {
        collector.block(result)
    } catch (ex: ValidationContext.StopValidationException) {
        null
    }
    if (collector.errors.isEmpty()) {
        return Validated(result, collector.errors.toList(), collector.warnings.toList())
    }
    return Validated(null, collector.errors.toList(), collector.warnings.toList())
}