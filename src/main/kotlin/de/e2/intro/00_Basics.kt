package de.e2.intro.basic

import kotlin.math.PI

fun add(a: Int, b: Int): Int {
    return a + b
}

fun sub(a: Int, b: Int) = a - b


fun main(args: Array<String>): Unit {
    //Funktionsaufrufe
    println("Start Intro")
    println(add(4, 5))
    println(sub(4, 5))

    //Unveränderbare Variablen
    val alter = 45
    val name = "Rene"
    val array = intArrayOf(7, 8, 9, 10)

    //Mutable und Imutable Collections
    val immutableList = listOf(1, 2, 3, 4)
//    immutableList.add(5) //Compile Error
    val mutableList = mutableListOf(1, 2, 3, 4)
    mutableList.add(5)

    //Veränderbare Variablen
    var heute = "Mittwoch"
    heute = "Donnerstag"

    //Funktion mit Default-Parameter
    printWaehrung(10)

    //Lambdas können ausserhalb der Klammer stehen
    val even = array.filter { value ->
        value % 2 == 0
    }

    //Lambdas mit einen Parameter gehe noch kürzer
    val odd = array.filter { it % 2 != 0 }

    //Objekte werden ohne new erzeugt
    val circle1 = Circle1(0, 0, 20)
    //Auf Properties kann direkt zugegriffen werden -> impliziter Getter, default public
    println(circle1.radius)
    println(circle1.getDurchmesser())

    val circle2 = Circle2(0, 0, 20)
    println(circle2.radius)
    //Properties ohne eigenes Atttribut sind möglich
    println(circle2.durchmesser)

    val circles = listOf(Circle3(radius = 10), Circle3(radius = 5), Circle3(radius = 7))
    //CircleRadiusComparator ist ein Objekt - Singleton
    val sortedCircles = circles.sortedWith(CircleRadiusComparator)

    //Interface -> Liste von Figuren
    val figures: List<Figure> = listOf(Circle4(0, 0, 5), Rectangle(0, 0, 10, 20))
}


fun printWaehrung(value: Int, symbol: String = "€"): Unit {
    //Strings können Variablen beinhalten - sogar Funktionsaufrufe
    println("Währung: $value ${symbol.trim()}")
}

//Die Parameter des primären Konstruktors werden direkt nach dem Klassennamen aufgelistet
class Circle1(x: Int, y: Int, radius: Int) {
    val x = x
    val y = y
    val radius = radius

    fun getDurchmesser() = radius * 2
}

//Mit val oder vars werden direkt Properties spezifiziert
class Circle2(val x: Int, val y: Int, val radius: Int) {

    //Bei Properties können getter und setter überschrieben werden
    val durchmesser
        get() = radius * 2
}

class Circle3(val x: Int=0, val y: Int=0, val radius: Int) {
    val durchmesser: Int

    //Standard Initialisierung
    init {
        durchmesser = radius * 2
    }

    //Weitere Konstruktoren sind möglich, aber meistens unnötig durch Default-Parameter
//    constructor(radius: Int) : this(0, 0, radius)
}

//Objekte sind Singletons
object CircleRadiusComparator : Comparator<Circle3> {
    //Explizites override notwendig
    override fun compare(o1: Circle3, o2: Circle3): Int = o1.radius - o2.radius
}

//Interfaces können Properties spezifizieren
interface Figure {
    val x: Int
    val y: Int

    //area() wäre natürlich auch ein schönes Property
    //Default Implementierungen sind möglich
    fun area(): Double = 0.0
}

//Explizites überschreiben von Properties notwendig
class Circle4(
    override val x: Int = 0,
    override val y: Int = 0,
    val radius: Int
) : Figure {
    val durchmesser
        get() = radius * 2

    override fun area(): Double = PI * durchmesser
}

class Rectangle(
    override val x: Int = 0,
    override val y: Int = 0,
    val width: Int,
    val height: Int
) : Figure {
    override fun area(): Double = (width * height).toDouble()
}
