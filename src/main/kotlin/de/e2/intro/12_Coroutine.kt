package de.e2.intro.coroutine

import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.util.concurrent.TimeUnit


fun main(args: Array<String>): Unit = runBlocking {
    for (id in 0..5) {
        launch(coroutineContext) {
            ping(id)
        }
    }
}

suspend fun ping(id: Int) {
    (1..10).forEach {
        println("$id: Ping from ${Thread.currentThread().name}")
        delay(1, TimeUnit.SECONDS)
    }
}