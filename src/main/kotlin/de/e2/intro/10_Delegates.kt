package de.e2.intro.delegates


data class Circle(
    val x: Int = 0,
    val y: Int = 0,
    val radius: Int = 0
) {
    //Lazy - wird beim ersten Zugriff berechnet
    val durchmesser: Int by lazy {
        radius * 2
    }
}

class DynamicEntity(val data: Map<String, Any>) {
    //Attribute werden aus der Map geholt
    val name: String by data
    val alter: String by data
}

//Entity ist gleichzeig eine Map
class DynamicEntityAndMap(val data: Map<String, Any>) : Map<String, Any> by data {
    val name: String by data
    val alter: String by data
}


fun main(args: Array<String>) {
    val data = mapOf("name" to "Rene", "alter" to 45)
    val entity = DynamicEntity(data)
    println(entity.name)

    val entityAndMap = DynamicEntityAndMap(data)
    println(entityAndMap["alter"])

    entityAndMap.size
}
