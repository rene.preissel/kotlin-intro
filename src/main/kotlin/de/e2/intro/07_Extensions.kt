package de.e2.intro.extensions

import java.net.URL

fun String.toURL() = URL(this)

class Person(val name: String, val age: Int)

data class PersonTO(val name: String, val age: Int)

fun Person.toTO(): PersonTO = PersonTO(name, age)

fun main(args: Array<String>) {
    //Extension-Funktionen erweitern vorhandene Klassen
    val url = "http://localhost".toURL()
    println(url)

    val rene = Person("Rene", 45)
    val reneTO = rene.toTO()

    println(reneTO)
}

