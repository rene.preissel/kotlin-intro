package de.e2.intro.helpers

class Person() {
    var name: String? = null
    var alter: Int? = null
}

fun main(args: Array<String>) {
    val person1 = Person()

    person1.alter = 45
    person1.name = "rene"

    val person2 = Person().apply {
        alter = 45
        name = "Rene"
    }

    val person3 = Person().also {
        it.alter = 45
        it.name = "Rene"
    }

    var person4: Person? = person3

    if(person4!=null) {
        val erg ="${person4.alter} - ${person4.name}"
    }

    //run() - this-Zeiger ist die Variable
    val alterUndName1 = person4?.run {
        "${alter} - ${name}"
    }

    //let() - it-Variable ist die Person
    val alterUndName2 = person4?.let {
        "${it.alter} - ${it.name}"
    }

    //Person ist nur dann ungleich null, wenn ein Alter Vorhanden ist
    val personMitAlter = person3.takeIf {  it.alter!=null }
}

