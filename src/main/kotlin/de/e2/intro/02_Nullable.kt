package de.e2.intro.nullable


class Adresse(val stadt: String)
class Person(val name: String, val geburtstname: String?, val adresse: Adresse?)

fun main(args: Array<String>) {
    val rene = Person("Rene Preissel", null, Adresse("Hamburg"))
    val anke = Person("Anke Preissel", "Baumann", Adresse("Hamburg"))
    val weihnachtsmann = Person("Santa Claus", null, null)


    //druckeAdresse(rene.adresse)  //compile error

    if (rene.adresse != null) {
        druckeAdresse(rene.adresse)
    }

    //?. ist Null-Sicher
    val stadt1: String? = anke.adresse?.stadt

    //?: wird benutzt wenn der Wert davor null ist -> Elvis Operator
    val stadt2: String = weihnachtsmann.adresse?.stadt ?: "unbekannt"

    //?: geht auch mit throw oder return
    val stadt3: String = rene.adresse?.stadt ?: throw IllegalStateException("Renes Adresse kann nicht null sein")
}

fun druckeAdresse(adresse: Adresse) {
    println(adresse.stadt)
}

