package de.e2.intro.smartcasts

import de.e2.intro.basic.Figure
import kotlin.math.PI

//Hierarchie von Figuren
interface Figure {
    val x: Int
    val y: Int

    fun area(): Double = 0.0
}

//Explizites Überschreiben der Properties
class Circle(
    override val x: Int = 0,
    override val y: Int = 0,
    val radius: Int
) : Figure {
    val durchmesser
        get() = radius * 2

    override fun area(): Double = PI * durchmesser
}

class Rectangle(
    override val x: Int = 0,
    override val y: Int = 0,
    val width: Int,
    val height: Int
) : Figure {
    override fun area(): Double = (width * height).toDouble()
}


fun main(args: Array<String>) {
    val figures = listOf(
        Circle(radius = 10),
        Circle(radius = 1),
        Rectangle(width = 10, height = 20)
    )
    for (figure in figures) {
        draw(figure)
    }
}

fun draw(figure: Figure) {
    //Nach der Abfrage is, wird der Typ der Variablen automatisch erkannt
    if (figure is Circle) {
        println("Circle ${figure.radius}")
    }

    if (figure is Rectangle) {
        println("Rectangle ${figure.width}x${figure.height}")
    }
}

fun drawWithWhen(figure: Figure) {
    //When unterscheidet auch per Typ: Ermöglicht zusammen mit Sealed-Klassen ADTs
    when (figure) {
        is Circle -> println("Circle ${figure.radius}")
        is Rectangle -> println("Rectangle ${figure.width}x${figure.height}")
        else -> println("Unbekannt")
    }
}

