# Agenda

## Warum Kotlin / Historie
* Entwickelt von JetBrains
    * wird in der eigenen Entwicklung benutzt  
    * 2011 veröffentlich, 2012 Apache2-Lizenz, 2016 Version 1.0
    * Scala wurde nicht benutzt, wegen zu langsamen Compiler
    * Kotlin ist eine Insel vor St. Petersburg
* Ziele: 
    * Statisch typisierte Programmiersprache
    * Unterstützung von objektorientierte und funktionaler Programmierung
    * Lesbarer Code mit weniger 'Noise' 
* Integration mit Java sehr gut
    * Mittlerweile geht auch transpilieren zu JavaScript
    * und zu nativen Code


## Grundlegende Syntax
* fun - Syntax
    * globale Funktionen
    * = Syntax
    * Funktionsaufrufe
* var / val
    * **Typisiert, aber Typangabe nicht notwendig, wenn der Compiler diesen ermitteln kann**
* **immutable vs mutable Collections**    
* **fun - Default Parameter**   
* **String-Templates**     
* Lambdas 
    * **Lambda-Blöcke als letztes Argument in Funktionen**
    * it-Parameter
* class
    * primary constructor und properties
    * **Properties ohne getter und setter aufrufen**
    * Objekte ohne new Erzeugen
* **object**
* interface
    * Explizites Überschreiben von Funktionen und Properties

    

## Features die den Unterschied machen
* Raw-Strings
* Nullable-Types,Null-Checks, Elvis-Operator
* Benannte Funktionsparameter
* Operator-Overloading
* Infix Funktionen 
* typealias
* data class
    * copy
    * destructing
* Destructing (Return von mehreren Ergebnissen oder Map)
* when-expression und if-expression
* smart casts
* Extension-Functions
* Lambdas with Receiver
* also, let, ...
* Delegating Properties
* Delegating Interfaces
* Names for Test methods - Backticks
* Koroutinen


## Ausserdem noch
* inline functions and reified
* companion object
* sealed classes
* named imports